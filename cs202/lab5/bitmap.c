#include <errno.h>

#include "disk_map.h"
#include "panic.h"
#include "bitmap.h"

// Check to see if the block bitmap indicates that block 'blockno' is free.
// Return 1 if the block is free, 0 if not.
bool
block_is_free(uint32_t blockno)
{
	if (super == 0 || blockno >= super->s_nblocks)
		return 0;
	if (bitmap[blockno / 32] & (1 << (blockno % 32)))
		return 1;
	return 0;
}

// Mark a block free in the bitmap. 
// Silently does nothing if passed blockno == 0
void
free_block(uint32_t blockno)
{
	// Blockno zero is the null pointer of block numbers.
	// Simplify code paths elsewhere by silently doing nothing
	// on free_block(0).
	if (blockno == 0)
		return; 

	bitmap[blockno/32] |= 1<<(blockno%32); // try to understand why
	// ~ the equivalent of exclamation point but for bit operations
	// the opposite of | is and not
}

// Search the bitmap for a free block and allocate it.  When you
// allocate a block, immediately flush the changed bitmap block
// to disk.
//
// Return block number allocated on success,
// -ENOSPC if we are out of blocks.
//
// Hint: use free_block as an example for manipulating the bitmap.
int
alloc_block(void)
{
	// The bitmap consists of one or more blocks.  A single bitmap block
	// contains the in-use bits for BLKBITSIZE blocks.  There are
	// super->s_nblocks blocks in the disk altogether.

	// LAB: Your code here.
	// panic("alloc_block not implemented");

	// Clear the bit of all the free disk blocks to indicate that 
	// they are in use and save the new bitmap on the disk

	// 2 D array, bitmap array of 32 bit numbers iterate over blocksize/32 then check if value > 0
	// iterate over each bit then change the first bit that is 1
	// to flush pass address & bitmap[i] 

	for (intptr_t i = 1; i < super->s_nblocks; i++){
		
		if ( block_is_free(i) ){
			// clear the ith bit in its respective 32 bit number
			// this indicates that it is used
			bitmap[i/32] &= ~(1<<(i%32)); 

			// find the address of the block that contains bit we changed and store the block on disk
			// skip the superblock with blkno 0
			flush_block( (void *) diskblock2memaddr( (i/(BLKBITSIZE))+1 ) ); 
			
			return i;
		}
	}

	return -ENOSPC;
}
