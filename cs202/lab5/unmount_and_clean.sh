#!/bin/bash

# unmount the driver
fusermount -u mnt

# create a message
echo 'sample' > build/msg

# make a clean testfs.img
test/makeimage.bash

# remove the symlink
rm -f mnt

# remove the mounting directory and its residents
rm -rf /lab5mnt

# recreate the mounting directory
mkdir /lab5mnt

# recreate the symlink
ln -s /lab5mnt mnt
