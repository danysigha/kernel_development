#include <cassert>

#include "EStore.h"

#include "sthread.h"

#include <cstdio>

using namespace std;

Item::
Item() : valid(false) 
{
}

Item::
~Item() 
{
}

EStore::
EStore(bool enableFineMode)
    : fineMode(enableFineMode)
{
    // TODO: Your code here.
    
    smutex_init(&store_lock); // initialize one lock for the whole Estore class
    scond_init(&store_cond); // initialize one conditional variable for the whole Estore class

    for (int i = 0; i<INVENTORY_SIZE; i++){
    inventory[i].valid = false;  // the inventory is initialized as an array of invalid items/ it is empty
    smutex_t item_lock;
    smutex_init(&item_lock); // initialize one mutex for each item when in finemode
    locks.push_back(item_lock);
    }
    
    shipping_cost = 3; 
    store_discount = 0; 
    
    
}

EStore::
~EStore()
{
    // TODO: Your code here.
    
    smutex_destroy(&store_lock); // free up the space used by the mutex and the conditional variable
    scond_destroy(&store_cond); 

    for (int i = 0; i<INVENTORY_SIZE; i++){
    smutex_destroy(&locks[i]); // free up the space used by each item's lock
    }
    
}

/*
 * ------------------------------------------------------------------
 * buyItem --
 *
 *      Attempt to buy the item from the store.
 *
 *      An item can be bought if:
 *          - The store carries it.
 *          - The item is in stock.
 *          - The cost of the item plus the cost of shipping is no
 *            more than the budget.
 *
 *      If the store *does not* carry this item, simply return and
 *      do nothing. Do not attempt to buy the item.
 *
 *      If the store *does* carry the item, but it is not in stock
 *      or its cost is over budget, block until both conditions are
 *      met (at which point the item should be bought) or the store
 *      removes the item from sale (at which point this method
 *      returns).
 *
 *      The overall cost of a purchase for a single item is defined
 *      as the current cost of the item times 1 - the store
 *      discount, plus the flat overall store shipping fee.
 *
 * Results:
 *      None.
 *
 * ------------------------------------------------------------------
 */
void EStore::
buyItem(int item_id, double budget)
{
    assert(!fineModeEnabled());

    // TODO: Your code here.
    smutex_lock(&store_lock);
    Item current_item = inventory[item_id];

    double overall_cost = (current_item.price * (1-current_item.discount) * (1 - store_discount)) + shipping_cost;
    // the overall cost is the price after the item and the store discounts plus the shipping cost

    while ( current_item.valid && ( (current_item.quantity<=0) || ( overall_cost > budget) ) ){
        //wait here if the store has the item but it has insufficient stock or the budget is insifficient
        scond_wait(&store_cond, &store_lock);
    }
    if (current_item.valid){ // ensure the item is available before we decrease quantity for purchase
        inventory[item_id].quantity -= 1;
    }
    
    smutex_unlock(&store_lock);
}

/*
 * ------------------------------------------------------------------
 * buyManyItem --
 *
 *      Attempt to buy all of the specified items at once. If the
 *      order cannot be bought, give up and return without buying
 *      anything. Otherwise buy the entire order at once.
 *
 *      The entire order can be bought if:
 *          - The store carries all items.
 *          - All items are in stock.
 *          - The cost of the the entire order (cost of items plus
 *            shipping for each item) is no more than the budget.
 *
 *      If multiple customers are attempting to buy at the same
 *      time and their orders are mutually exclusive (i.e., the
 *      two customers are not trying to buy any of the same items),
 *      then their orders must be processed at the same time.
 *
 *      For the purposes of this lab, it is OK for the store
 *      discount and shipping cost to change while an order is being
 *      processed.
 *
 *      The cost of a purchase of many items is the sum of the
 *      costs of purchasing each item individually. The purchase
 *      cost of an individual item is covered above in the
 *      description of buyItem.
 *
 *      Challenge: For bonus points, implement a version of this
 *      method that will wait until the order can be fulfilled
 *      instead of giving up. The implementation should be efficient
 *      in that it should not wake up threads unecessarily. For
 *      instance, if an item decreases in price, only threads that
 *      are waiting to buy an order that includes that item should be
 *      signaled (though all such threads should be signaled).
 *
 *      Challenge: For bonus points, ensure that the shipping cost
 *      and store discount does not change while processing an
 *      order.
 *
 * Results:
 *      None.
 *
 * ------------------------------------------------------------------
 */
void EStore::
buyManyItems(vector<int>* item_ids, double budget)
{
    assert(fineModeEnabled());

    // TODO: Your code here.
    bool offer_items = true;
    bool in_stock = true;
    bool enough_budget = true;
    int total_cost = 0;
    int i;

    for (i = 0; i< (int)item_ids->size(); i++){ // acquiring the mutexes of all the items
        // this ensures we acquire mutexes and free all the mutexes that we acquired in the same order
        smutex_lock( &locks[(*item_ids)[i]] ); 

        if ( !inventory[(*item_ids)[i]].valid ){
            offer_items = false; // flag to note the order cannot proceed
        }
        if ( inventory[(*item_ids)[i]].quantity <= 0 ){
            in_stock = false;
        }

        total_cost += (( inventory[(*item_ids)[i]].price * (1-inventory[(*item_ids)[i]].discount) * (1 - store_discount) ) + shipping_cost);
        // add to the total cost the price of each item after the item and the store discounts and each item's shipping cost
    }

    if ( total_cost > budget ){ 
        enough_budget = false;
    }


    if (offer_items && in_stock && enough_budget){ // change stock only if we can purchase
        budget -= total_cost;
        for(i = 0; i< (int)item_ids->size(); i++){
            inventory[(*item_ids)[i]].quantity -= 1;
        }
    }

    for(i = 0; i< (int)item_ids->size(); i++){ // must release all locks for next thread to process
        smutex_unlock( &locks[(*item_ids)[i]] );
    }
}

/*
 * ------------------------------------------------------------------
 * addItem --
 *
 *      Add the item to the store with the specified quantity,
 *      price, and discount. If the store already carries an item
 *      with the specified id, do nothing.
 *
 * Results:
 *      None.
 *
 * ------------------------------------------------------------------
 */
void EStore::
addItem(int item_id, int quantity, double price, double discount)
{
    // TODO: Your code here.
    smutex_t cur_lock;
    
    if (fineModeEnabled()){
        cur_lock = locks[item_id]; // use item lock
    }else{
        cur_lock = store_lock; // use store lock
    }

    smutex_lock(&cur_lock);
        if (!inventory[item_id].valid){ // set the fields of the item object
            inventory[item_id].valid = true;
            inventory[item_id].discount = discount;
            inventory[item_id].price = price;
            inventory[item_id].quantity = quantity;
        }
    smutex_unlock(&cur_lock);
}

/*
 * ------------------------------------------------------------------
 * removeItem --
 *
 *      Remove the item from the store. The store no longer carries
 *      this item. If the store is not carrying this item, do
 *      nothing.
 *
 *      Wake any waiters. // why do I need to wake up something
 *
 * Results:
 *      None.
 *
 * ------------------------------------------------------------------
 */
void EStore::
removeItem(int item_id)
{
    // TODO: Your code here.
    smutex_t cur_lock;
    if (fineModeEnabled()){
        cur_lock = locks[item_id];
    }else{
        cur_lock = store_lock;
    }

    smutex_lock(&cur_lock);
    inventory[item_id].valid = false;
    scond_broadcast(&store_cond, &cur_lock);
    smutex_unlock(&cur_lock);
}

/*
 * ------------------------------------------------------------------
 * addStock --
 *
 *      Increase the stock of the specified item by count. If the
 *      store does not carry the item, do nothing. Wake any waiters.
 *
 * Results:
 *      None.
 *
 * ------------------------------------------------------------------
 */
void EStore::
addStock(int item_id, int count)
{
    // TODO: Your code here.
    smutex_t cur_lock;
    if (fineModeEnabled()){
        cur_lock = locks[item_id];
    }else{
        cur_lock = store_lock;
    }
    
    smutex_lock(&cur_lock);
    inventory[item_id].quantity += count;
    scond_broadcast(&store_cond, &cur_lock);
    smutex_unlock(&cur_lock);
}

/*
 * ------------------------------------------------------------------
 * priceItem --
 *
 *      Change the price on the item. If the store does not carry
 *      the item, do nothing.
 *
 *      If the item price decreased, wake any waiters.
 *
 * Results:
 *      None.
 *
 * ------------------------------------------------------------------
 */
void EStore::
priceItem(int item_id, double price)
{
    // TODO: Your code here.
    smutex_t cur_lock;
    if (fineModeEnabled()){
        cur_lock = locks[item_id];
    }else{
        cur_lock = store_lock;
    }

    smutex_lock(&cur_lock);
    
    if (inventory[item_id].valid){
        int prev_price = inventory[item_id].price;
        inventory[item_id].price = price;
        if (prev_price < inventory[item_id].price ){ // only wake waiters if the price increased
            scond_broadcast( &store_cond, &cur_lock);
        }
    }
    smutex_unlock(&cur_lock);
}

/*
 * ------------------------------------------------------------------
 * discountItem --
 *
 *      Change the discount on the item. If the store does not carry
 *      the item, do nothing.
 *
 *      If the item discount increased, wake any waiters.
 *
 * Results:
 *      None.
 *
 * ------------------------------------------------------------------
 */
void EStore::
discountItem(int item_id, double discount)
{
    // TODO: Your code here.
    smutex_t cur_lock;
    if (fineModeEnabled()){
        cur_lock = locks[item_id];
    }else{
        cur_lock = store_lock;
    }

    smutex_lock(&cur_lock);
    
    if (inventory[item_id].valid){
        int prev_discount = inventory[item_id].discount;
        inventory[item_id].discount = discount;
        if (prev_discount < inventory[item_id].discount){ // only wake waiters if the item discount increased
            scond_broadcast(&store_cond, &cur_lock);
        }
    }
    smutex_unlock(&cur_lock);
}

/*
 * ------------------------------------------------------------------
 * setShippingCost --
 *
 *      Set the per-item shipping cost. If the shipping cost
 *      decreased, wake any waiters.
 *
 * Results:
 *      None.
 *
 * ------------------------------------------------------------------
 */
void EStore::
setShippingCost(double cost)
{
    // TODO: Your code here.
    smutex_lock(&store_lock);
    int prev_cost = shipping_cost;
    shipping_cost = cost;
    if ( prev_cost > shipping_cost){ 
        scond_broadcast( &store_cond, &store_lock); // only wake waiters if the shipping cost decreased
    }
    smutex_unlock(&store_lock);
}

/*
 * ------------------------------------------------------------------
 * setStoreDiscount --
 *
 *      Set the store discount. If the discount increased, wake any
 *      waiters.
 *
 * Results:
 *      None.
 *
 * ------------------------------------------------------------------
 */
void EStore::
setStoreDiscount(double discount)
{
    // TODO: Your code here.
    smutex_lock(&store_lock); 
    int prev_discount = store_discount;
    store_discount = discount;
    if ( prev_discount < store_discount){ 
        scond_broadcast( &store_cond, &store_lock); // only wake waiters if the store discount increased
    }
    smutex_unlock(&store_lock);
}
