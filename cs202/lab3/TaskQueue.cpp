
#include "TaskQueue.h"

#include <cassert>

TaskQueue::
TaskQueue() 
{
    // TODO: Your code here.
    smutex_init(&mutex); // initialize lock 
    scond_init(&empty_deque); // initialize cond variable 
    deque_size = 0;
}

TaskQueue::
~TaskQueue() 
{
    // TODO: Your code here.
    smutex_destroy(&mutex); // destroying mutex and conditional variable 
    scond_destroy(&empty_deque);
}

/*
 * ------------------------------------------------------------------
 * size --
 *
 *      Return the current size of the queue.
 *
 * Results:
 *      The size of the queue.
 *
 * ------------------------------------------------------------------
 */
int TaskQueue::
size()
{
    // TODO: Your code here.

    return deque_size; // no need for lock here because we already have it

    // deque.size() is not thread safe. Keep track of the size manually
    // when enqueue, deque_size += 1; when dequeue, deque_size-=1
}

/*
 * ------------------------------------------------------------------
 * empty --
 *
 *      Return whether or not the queue is empty.
 *
 * Results:
 *      The true if the queue is empty and false otherwise.
 *
 * ------------------------------------------------------------------
 */
bool TaskQueue::
empty()
{
    // TODO: Your code here.

    return (size() == 0); // no need for lock here because we already have it
}

/*
 * ------------------------------------------------------------------
 * enqueue --
 *
 *      Insert the task at the back of the queue.
 *
 * Results:
 *      None.
 *
 * ------------------------------------------------------------------
 */
void TaskQueue::
enqueue(Task task)
{
    // TODO: Your code here.
    smutex_lock(&mutex); // use lock to access shared datastructure safely
    task_list.push_back(task);
    deque_size += 1;
    scond_signal(&empty_deque, &mutex); // signal not empty to unblock other threads
    smutex_unlock(&mutex);
} 

/*
 * ------------------------------------------------------------------
 * dequeue --
 *
 *      Remove the Task at the front of the queue and return it.
 *      If the queue is empty, block until a Task is inserted.
 *
 * Results:
 *      The Task at the front of the queue.
 *
 * ------------------------------------------------------------------
 */
Task TaskQueue::
dequeue()
{
    // TODO: Your code here.
    smutex_lock(&mutex);
    while (empty()){ // wait if the deque is empty
        scond_wait(&empty_deque, &mutex);
    }

    Task top = task_list.front(); // retrieve the fisrt task in the deque for execution
    task_list.pop_front();
    deque_size -= 1;

    smutex_unlock(&mutex);
    //unlock - we can unlock here because we are done modifying the datastructure
    // note that there may be an interleaving but it does not modify core behavior
    return top; 
}