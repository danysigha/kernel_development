#include <cstring>
#include <cstdlib>

#include "EStore.h"
#include "TaskQueue.h"

#include "RequestGenerator.h"

class Simulation
{
    public:
    TaskQueue supplierTasks;
    TaskQueue customerTasks;
    EStore store;

    int maxTasks;
    int numSuppliers;
    int numCustomers;

    explicit Simulation(bool useFineMode) : store(useFineMode) { }
};

/*
 * ------------------------------------------------------------------
 * supplierGenerator --
 *
 *      The supplier generator thread. The argument is a pointer to
 *      the shared Simulation object.
 *
 *      Enqueue arg->maxTasks requests to the supplier queue, then
 *      stop all supplier threads by enqueuing arg->numSuppliers
 *      stop requests.
 *
 *      Use a SupplierRequestGenerator to generate and enqueue
 *      requests.
 *
 *      This thread should exit when done.
 *
 * Results:
 *      Does not return. Exit instead.
 *
 * ------------------------------------------------------------------
 */

static void*
supplierGenerator(void* arg)
{
    Simulation* sim = (Simulation*)arg;
    // instantiate the SupplierRequestGenerator class
    SupplierRequestGenerator sup( &sim->supplierTasks);
    
    sup.enqueueTasks(sim->maxTasks, &sim->store); // add maxTasks tasks to the deque of supplier tasks
    sup.enqueueStops(sim->numSuppliers); // add tasks to make the threads that execute tasks stop
    
    sthread_exit();
    
    return NULL; // Keep compiler happy.
}

/*
 * ------------------------------------------------------------------
 * customerGenerator --
 *
 *      The customer generator thread. The argument is a pointer to
 *      the shared Simulation object.
 *
 *      Enqueue arg->maxTasks requests to the customer queue, then
 *      stop all customer threads by enqueuing arg->numCustomers
 *      stop requests.
 *
 *      Use a CustomerRequestGenerator to generate and enqueue
 *      requests.  For the fineMode argument to the constructor
 *      of CustomerRequestGenerator, use the output of
 *      store.fineModeEnabled() method, where store is a field
 *      in the Simulation class.
 *
 *      This thread should exit when done.
 *
 * Results:
 *      Does not return. Exit instead.
 *
 * ------------------------------------------------------------------
 */
static void*
customerGenerator(void* arg) 
{
    // TODO: Your code here.

    // instantiate the CustomerRequestGenerator class     
    CustomerRequestGenerator cust( &(((Simulation*)arg)->customerTasks), ((Simulation*)arg)->store.fineModeEnabled() );
    
    cust.enqueueTasks( ((Simulation*)arg)->maxTasks, &((Simulation*)arg)->store); // add maxTasks tasks to the deque of customer tasks
    cust.enqueueStops(((Simulation*)arg)->numCustomers); // add tasks to make the threads that execute tasks stop
    
    sthread_exit();    

    return NULL; // Keep compiler happy.
}

/*
 * ------------------------------------------------------------------
 * supplier --
 *
 *      The main supplier thread. The argument is a pointer to the
 *      shared Simulation object.
 *
 *      Dequeue Tasks from the supplier queue and execute them.
 *
 * Results:
 *      Does not return.
 *
 * ------------------------------------------------------------------
 */
static void*
supplier(void* arg)
{
    // TODO: Your code here.
    // the function is called in a specific thread
    // the function executes tasks in the deque of supplier tasks
    // stops only when executing tasks that make the thread exit
    while(1){ 
        Task cur_task = ((Simulation*)arg)->supplierTasks.dequeue(); // get a task
        cur_task.handler(cur_task.arg); //execute the task
    }
    
    return NULL; // Keep compiler happy.
}

/*
 * ------------------------------------------------------------------
 * customer --
 *
 *      The main customer thread. The argument is a pointer to the
 *      shared Simulation object.
 *
 *      Dequeue Tasks from the customer queue and execute them.
 *
 * Results:
 *      Does not return.
 *
 * ------------------------------------------------------------------
 */
static void*
customer(void* arg)
{
    // TODO: Your code here.
    // the function is called in a specific thread
    // the function executes tasks in the deque of customer tasks
    // stops only when executing tasks that make the thread exit
    printf("A customer thread.\n");
    while(1){
        Task cur_task = ((Simulation*)arg)->customerTasks.dequeue(); // get a task
        cur_task.handler(cur_task.arg); //execute the task
    }
    return NULL; // Keep compiler happy.
}

/*
 * ------------------------------------------------------------------
 * startSimulation --
 *      Create a new Simulation object. This object will serve as
 *      the shared state for the simulation. 
 *
 *      Create the following threads:
 *          - 1 supplier generator thread.
 *          - 1 customer generator thread.
 *          - numSuppliers supplier threads.
 *          - numCustomers customer threads.
 *
 *      After creating the worker threads, the main thread
 *      should wait until all of them exit, at which point it
 *      should return.
 *
 *      Hint: Use sthread_join.
 *
 * Results:
 *      None.
 *
 * ------------------------------------------------------------------
 */
static void
startSimulation(int numSuppliers, int numCustomers, int maxTasks, bool useFineMode)
{
    // TODO: Your code here.
    sthread_t sup_generator; // the thread that adds supplier tasks to the supplier tasks deque
    sthread_t cust_generator; // the thread that adds customer tasks to the cutomer tasks deque

    Simulation simulation_objc(useFineMode);
    simulation_objc.maxTasks = maxTasks;
    simulation_objc.numCustomers = numCustomers;
    simulation_objc.numSuppliers = numSuppliers;
    sthread_t* supplier_threads = new sthread_t[numSuppliers]; // the threads that execute supplier tasks
    sthread_t* customer_threads = new sthread_t[numCustomers]; // the threads that execute cutomer tasks
    int i;

    sthread_create(&sup_generator,
		    &supplierGenerator, 
		    &simulation_objc);

    sthread_create(&cust_generator,
		    &customerGenerator, 
		    &simulation_objc);

    for (i = 0; i<numSuppliers; i++){
        sthread_create(&supplier_threads[i],
		    supplier, 
		    &simulation_objc);
    }

    for ( i=0 ; i<numCustomers; i++){
        sthread_create(&customer_threads[i],
		    customer, 
		    &simulation_objc);
    }

    sthread_join(sup_generator); // both threads must stop before the function proceeds
    sthread_join(cust_generator);


    for ( i=0 ; i<numSuppliers; i++){
        sthread_join(supplier_threads[i]);
    } // all threads must stop before the function proceeds

    for ( i=0 ; i<numCustomers; i++){
        sthread_join(customer_threads[i]);
    } // all threads must stop before the function proceeds

}

int main(int argc, char **argv)
{
    bool useFineMode = false;

    // Seed the random number generator.
    // You can remove this line or set it to some constant to get deterministic
    // results, but make sure you put it back before turning in.
    srand(time(NULL));

    if (argc > 1)
        useFineMode = strcmp(argv[1], "--fine") == 0;
    startSimulation(10, 10, 100, useFineMode);
    return 0;
}

