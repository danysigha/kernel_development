#include <stdio.h>

int isitcorrect(char myword[], char myletters[]){
  int i, dict_word[256] = {}, dict_letters[256] = {};

  for (i=0; myword[i]!='\0'; i++){
    dict_word[myword[i]]++;
  }

  for (i=0; myletters[i]!='\0'; i++){
    dict_letters[myletters[i]]++;
  }

  for (i=0; i<256; i++){
    if (dict_word[i] != dict_letters[i]){
      return 0;
    }
  }
  return 1;
}

int correct_word (char myword[], char myletters[])
{
  int i, inv_word[256] = {}, inv_letters[256] = {};
// construct the two inventories
  for (i = 0; myword[i] != '\0'; i++)
    inv_word[myword[i]]++;
  for (i = 0; myletters[i] != '\0'; i++)
    inv_letters[myletters[i]]++;
// verification
  for (i = 0; i < 256 ; i++){
  if (inv_word[i] != inv_letters[i])
  return 0;
  }
  return 1;
}


int main(){
  char myword[] = "This is my world";
  char myletters[] = "This is my world";
  printf("%d\n", isitcorrect(myword, myletters));
  return 0;
}
