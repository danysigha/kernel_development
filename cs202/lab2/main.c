#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <grp.h>
#include <pwd.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#include <dirent.h>
#include <sys/param.h>
#include <stdint.h>

static int err_code;
static int file_count = 0; // global variable to keep track of file count

/*
 * here are some function signatures and macros that may be helpful.
 */

void handle_error(char* fullname, char* action);
bool test_file(char* pathandname);
bool is_dir(char* pathandname);
const char* ftype_to_str(mode_t mode);
void list_file(char* pathandname, char* name, bool list_long);
void list_dir(char* dirname, bool list_long, bool list_all, bool recursive, bool print_count);

/*
 * You can use the NOT_YET_IMPLEMENTED macro to error out when you reach parts
 * of the code you have not yet finished implementing.
 */
#define NOT_YET_IMPLEMENTED(msg)                  \
    do {                                          \
        printf("Not yet implemented: " msg "\n"); \
        exit(255);                                \
    } while (0)

/*
 * PRINT_ERROR: This can be used to print the cause of an error returned by a
 * system call. It can help with debugging and reporting error causes to
 * the user. Example usage:
 *     if ( error_condition ) {
 *        PRINT_ERROR();
 *     }
 */
#define PRINT_ERROR(progname, what_happened, pathandname)               \
    do {                                                                \
        printf("%s: %s %s: %s\n", progname, what_happened, pathandname, \
               strerror(errno));                                        \
    } while (0)

/* PRINT_PERM_CHAR:
 *
 * This will be useful for -l permission printing.  It prints the given
 * 'ch' if the permission exists, or "-" otherwise.
 * Example usage:
 *     PRINT_PERM_CHAR(sb.st_mode, S_IRUSR, "r"); // make sure you use a string
 */
#define PRINT_PERM_CHAR(mode, mask, ch) printf("%s", (mode & mask) ? ch : "-");

/*
 * Get username for uid. Return 1 on failure, 0 otherwise.
 */
static int uname_for_uid(uid_t uid, char* buf, size_t buflen) {
    struct passwd* p = getpwuid(uid);
    if (p == NULL) {
        return 1;
    }
    strncpy(buf, p->pw_name, buflen);
    return 0;
}

/*
 * Get group name for gid. Return 1 on failure, 0 otherwise.
 */
static int group_for_gid(gid_t gid, char* buf, size_t buflen) {
    struct group* g = getgrgid(gid);
    if (g == NULL) {
        return 1;
    }
    strncpy(buf, g->gr_name, buflen);
    return 0;
}

/*
 * Format the supplied `struct timespec` in `ts` (e.g., from `stat.st_mtim`) as a
 * string in `char *out`. Returns the length of the formatted string (see, `man
 * 3 strftime`).
 */
static size_t date_string(struct timespec* ts, char* out, size_t len) {
    struct timespec now;
    timespec_get(&now, TIME_UTC);
    struct tm* t = localtime(&ts->tv_sec);
    if (now.tv_sec < ts->tv_sec) {
        // Future time, treat with care.
        return strftime(out, len, "%b %e %Y", t);
    } else {
        time_t difference = now.tv_sec - ts->tv_sec;
        if (difference < 31556952ull) {
            return strftime(out, len, "%b %e %H:%M", t);
        } else {
            return strftime(out, len, "%b %e %Y", t);
        }
    }
}

/*
 * Print help message and exit.
 */
static void help() {
    /* TODO: add to this */
    printf("ls: List files\n");
    printf("\t--help: Print this help\n");
    printf("ls: List all files in current directory.\n");
    printf("ls-a: List all files including hidden ones in current directory.\n");
    printf("ls-R: List all files in current directory and list files in other directories recursively.\n");
    printf("ls-l: List all files with additional information such as user and user group permissions, file size, and last modified date.\n");
    printf("ls-n: Show a count of all the files.\n");
    exit(0);
}

/*
 * call this when there's been an error.
 * The function should:
 * - print a suitable error message (this is already implemented)
 * - set appropriate bits in err_code
 */
void handle_error(char* what_happened, char* fullname) {
    PRINT_ERROR("ls", what_happened, fullname);

    // TODO: your code here: inspect errno and set err_code accordingly.
    if (err_code == 0){
        err_code |= 0x40; // setting the relevant bit to true
    }

    if (errno == ENOENT){
        err_code |= 0x08; // setting the relevant bit to true
        // err_code = 0b1001000;
    }

    else if (errno == EACCES){
        err_code |= 0x10; // setting the relevant bit to true
        // err_code = 0b1010000;
    }

    return;
}

/*
 * test_file():
 * test whether stat() returns successfully and if not, handle error.
 * Use this to test for whether a file or dir exists
 */
bool test_file(char* pathandname) {
    struct stat sb;
    if (stat(pathandname, &sb)) {
        handle_error("cannot access", pathandname);
        return false;
    }
    return true;
}

/*
 * is_dir(): tests whether the argument refers to a directory.
 * precondition: test_file() returns true. that is, call this function
 * only if test_file(pathandname) returned true.
 */
bool is_dir(char* pathandname) {
    /* TODO: fillin */
    struct stat sb; // 
    stat(pathandname, &sb);
    
    if ( S_ISDIR(sb.st_mode) ){ // use system call to check if the file is a directory
        return true;
    }
    return false;
}

/* convert the mode field in a struct stat to a file type, for -l printing */
const char* ftype_to_str(mode_t mode) {
    /* TODO: fillin */

    return "?";
}

/* list_file():
 * implement the logic for listing a single file.
 * This function takes:
 *   - pathandname: the directory name plus the file name.
 *   - name: just the name "component".
 *   - list_long: a flag indicated whether the printout should be in
 *   long mode.
 *
 *   The reason for this signature is convenience: some of the file-outputting
 *   logic requires the full pathandname (specifically, testing for a directory
 *   so you can print a '/' and outputting in long mode), and some of it
 *   requires only the 'name' part. So we pass in both. An alternative
 *   implementation would pass in pathandname and parse out 'name'.
 */
void list_file(char* pathandname, char* name, bool list_long) {
    /* TODO: fill in*/
    struct stat sb; 
    stat(pathandname, &sb);
    char record[MAXPATHLEN];
    char permission[11];
    char user_name[20];
    char group_name[20];
    char time[256];
    int user_flag;
    int group_flag;
    int links = sb.st_nlink;
    int shift = 1;
    // struct timespec ts;

    // for (int i = 0; i<10; i++){}

    if (list_long){

        if ( S_ISREG(sb.st_mode) ){ // use bit mask defined in man pages to shift relevant bits and determine permissions
            permission[0] = '-';
        }else if ( S_ISDIR(sb.st_mode) )
        {
            permission[0] = 'd';
        }else{
            permission[0] = '?';
        }
        
        if ( (sb.st_mode & S_IRUSR ) ) {
            permission[1] = 'r';
        }else{
            permission[1] = '-';
        }
        if ( (sb.st_mode & S_IWUSR) ) {
            permission[2] = 'w';
        }else{
            permission[2] = '-';
        }
        if ( (sb.st_mode & S_IXUSR) ) {
            permission[3] = 'x';
        }else{
            permission[3] = '-';
        }

        if ( (sb.st_mode & S_IRGRP) ) {
            permission[4] = 'r';
        }else{
            permission[4] = '-';
        }
        if (sb.st_mode & S_IWGRP ) {
            permission[5] = 'w';
        }else{
            permission[5] = '-';
        }
        if (sb.st_mode & S_IXGRP ) {
            permission[6] = 'x';
        }else{
            permission[6] = '-';
        }

        if (sb.st_mode & shift<<2 ) { // no bit mask defined for other groups in man pages so I manually verify last three bits
            permission[7] = 'r';
        }else{
            permission[7] = '-';
        }
        if (sb.st_mode & shift<<1 ) {
            permission[8] = 'w';
        }else{
            permission[8] = '-';
        }
        if (sb.st_mode & shift ) {
            permission[9] = 'x';
        }else{
            permission[9] = '-';
        }

        permission[10] = '\0';


        user_flag = uname_for_uid(sb.st_uid, user_name, 20); // collect info about whether user or group were retrieved succeffully
        group_flag = group_for_gid(sb.st_gid, group_name, 20); 

        struct timespec ts;
        ts.tv_sec = sb.st_mtime;
        date_string(&ts, time, 256);
        // void *user;
        // void *group;

        if (group_flag){
            sprintf(group_name, "%d", sb.st_gid);
            // group = g_name;
        }
        
        if (user_flag){
            sprintf(user_name, "%d", sb.st_uid);
            // user = &u_name;
        }
        // set the ls -l content
        snprintf(record, MAXPATHLEN, "%s %d %s %s %ld %s %s", permission, links, user_name, group_name, sb.st_size, time, name);

        if (user_flag){
            err_code |= 0x20; // bit shifting for error when acquiring user id
            if (err_code == 0){
                err_code |= 0x40;
            }
        }

        if (group_flag){
            if (!user_flag){
                err_code |= 0x20; // bit shifting for error when acquiring group id
                }
            if (err_code == 0){
                err_code |= 0x40;
            }
        }

        (void) printf("%s\n", record);
        
        
    }else{
        (void) printf("%s\n", name); // case when not in long mode
    }
    
}

/* list_dir():
 * implement the logic for listing a directory.
 * This function takes:
 *    - dirname: the name of the directory
 *    - list_long: should the directory be listed in long mode?
 *    - list_all: are we in "-a" mode?
 *    - recursive: are we supposed to list sub-directories?
 */
void list_dir(char* dirname, bool list_long, bool list_all, bool recursive, bool print_count) {
    /* TODO: fill in
     *   You'll probably want to make use of:
     *       opendir()
     *       readdir()
     *       list_file()
     *       snprintf() [to make the 'pathandname' argument to
     *          list_file(). that requires concatenating 'dirname' and
     *          the 'd_name' portion of the dirents]
     *       closedir()
     *   See the lab description for further hints
     */

    DIR *dirp;
    struct dirent *dp;
    
    if ((dirp = opendir(dirname)) == NULL) { // this opens directory and passes pointer to directory stream
        char whathappned[MAXPATHLEN];
        char msg[] = "cannot open directory";
        snprintf(whathappned, MAXPATHLEN, "%s %s: Permission denied.", msg, dirname);
        handle_error(whathappned, dirname);
        return;
    }

    do {
        errno = 0;
        if ((dp = readdir(dirp)) != NULL) { // returns a pointer to the NEXT directory entry
        // this is what to use to jump from file to file
            
            char pathandname[MAXPATHLEN];
            snprintf(pathandname, MAXPATHLEN, "%s/%s", dirname, dp->d_name);
            // printf("%s\n", pathandname);
            
            if (!list_all && dp->d_name[0] == '.'){
                continue;
            }

            if (print_count){
                file_count+=1;
                continue;
            }

            // if (recursive){
            //     snprintf(name, MAXPATHLEN, "%s/", dp->d_name);
            // }
            
            char subd[] = ".";
            char subd2[] = "..";

            if ( (dp->d_type == 4) && strcmp(subd, dp->d_name) && strcmp(subd2, dp->d_name) ){ // check the type of the file   
            // and check if not . or .. to avoid printing ./ or ../
                char name[MAXPATHLEN]; 
                snprintf(name, MAXPATHLEN, "%s/", dp->d_name);
                list_file(pathandname, name, list_long);
            }else{
                list_file(pathandname, dp->d_name, list_long);
            }
            
        }
    } while (dp != NULL);

    if (recursive){
        
        
        rewinddir(dirp); // go back to first file in current directory
    
        do {
            errno = 0;
            if ((dp = readdir(dirp)) != NULL) { // returns a pointer to the NEXT directory entry
            // this is what to use to jump from directory to directory (file to file??)
                
                char pathandname[MAXPATHLEN];
                snprintf(pathandname, MAXPATHLEN, "%s/%s", dirname, dp->d_name);
                // printf("%s\n", pathandname);
                
                if (!list_all && dp->d_name[0] == '.'){
                    continue;
                }

                // if (recursive){
                //     snprintf(name, MAXPATHLEN, "%s/", dp->d_name);
                // }

                char subd[] = ".";
                char subd2[] = "..";

                
                if ( (dp->d_type == 4) && strcmp(subd, dp->d_name) && strcmp(subd2, dp->d_name) ){ 
                    // line to avoid recursing forever and ensure you only recurse into directories
                    
                    if (!print_count){
                        printf("\n%s:\n", pathandname);
                    }
                    
                    list_dir(pathandname, list_long, list_all, recursive, print_count); // recursive call in a new directory
                }
            }
        } while (dp != NULL); // loop to handle recursive case

    }
    
    (void) closedir(dirp);
    return;
}

int main(int argc, char* argv[]) {
    // This needs to be int since C does not specify whether char is signed or
    // unsigned.
    int opt;
    err_code = 0;
    bool list_long = false, list_all = false, print_count = false, recursive = false;
    // We make use of getopt_long for argument parsing, and this
    // (single-element) array is used as input to that function. The `struct
    // option` helps us parse arguments of the form `--FOO`. Refer to `man 3
    // getopt_long` for more information.
    struct option opts[] = {
        {.name = "help", .has_arg = 0, .flag = NULL, .val = '\a'}};

    // This loop is used for argument parsing. Refer to `man 3 getopt_long` to
    // better understand what is going on here.
    while ((opt = getopt_long(argc, argv, "1alnR", opts, NULL)) != -1) {
        switch (opt) {
            case '\a':
                // Handle the case that the user passed in `--help`. (In the
                // long argument array above, we used '\a' to indicate this
                // case.)
                help();
                break;
            case '1':
                // Safe to ignore since this is default behavior for our version
                // of ls.
                break;
            case 'a':
                list_all = true;
                break;
                // TODO: you will need to add items here to handle the
                // cases that the user enters "-l" or "-R"
            case 'l':
                list_long = true;
                break;
            case 'n':
                print_count = true;
                break;
            case 'R':
                recursive = true;
                break;
            default:
                printf("Unimplemented flag %d\n", opt);
                break;
        }
    }

    // TODO: Replace this.
    //if (optind < argc) { // the variable optind contains the index to the next argv argument for a subsequent call to getopt()
    //    printf("Optional arguments: ");
    //}
    //for (int i = optind; i < argc; i++) {
    //    printf("%s ", argv[i]);
    //}
    //if (optind < argc) {
    //    printf("\n");
    //}

    
    char cwd[] = ".";

    if (optind < argc){
        for (int i = optind; i < argc; i++) { // loop to go over all arguments
            char name[MAXPATHLEN];
            
            snprintf(name, MAXPATHLEN, "%s/%s", cwd, argv[i]);

            if (test_file(name)){

                if (is_dir(name)){

                    if (!print_count){
                        if (!list_long && recursive){ // condition to ensure formating matches the current mode
                            printf("%s:\n", argv[i]);
                        }else if(list_long && recursive){
                            printf("%s:\n", argv[i]);
                        }
                    }
                    
                    list_dir(argv[i], list_long, list_all, recursive, print_count);
                }else{
                    printf("%s\n", argv[i]);
                }
            }
        }
    }else{
        list_dir(cwd, list_long, list_all, recursive, print_count);
    }

    if (print_count){
        printf("%d\n", file_count);
    }

    // NOT_YET_IMPLEMENTED("Listing files");
    exit(err_code);
}