#include <stdio.h>

int main(int argc, char** argv){

    char* arg; 
    int i, c, count=0; 
    c = 'a';
    arg = argv[1]; // need to make sure we got th right number of arguments
    
    if (argc != 2){
        printf("usage: %s <arg1> \n", argv[0]);
        return 0;
    } // check if we got two arguments at funciton call

    for (i=0; arg[i]!='\0'; i++){
        if (arg[i] == c){
            count++;
        }
    } // go over all the characters and increment the count whenever we encounter 'a'
    printf("%d\n", count);
    return 0;
}