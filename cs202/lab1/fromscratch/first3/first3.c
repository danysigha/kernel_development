#include <stdio.h>
#include <string.h>

int main(int argc, char** argv){

    char* first_arg; 
    char* second_arg;
    int i; 
    first_arg = argv[1];
    second_arg = argv[2];
    
    if (argc != 3){
        printf("usage: %s <arg1> <arg2>\n", argv[0]);
        return 0;
    } // checking if we have received two arguments at function call

    if ( ( strlen(first_arg) != 3 ) || ( strlen(second_arg) != 3 ) ){
        printf("%s : error : One or more arguments have fewer than 3 characters\n", argv[0]);
        return 0; // if the lenght of any of the two arguments is less than 3 give feedback to user
    }else{
        for (i=0; i<3; i++){
                printf("%c", first_arg[i]);
            } 
        for (i=0; i<3; i++){
                printf("%c", second_arg[i]);
            } // print the content of both arguments
        printf("\n");
        return 0;
    }
}
    
