#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "part3.h"

// you have to declare the func_name function signature here, otherwise, the compiler would complain
// The function is defined in part1.c

extern struct list_node *alloc_node(void);
extern void free_node(struct list_node *node);
extern void list_insert(struct list_node *head, int value);
extern struct list_node * list_end(struct list_node *head);
extern int list_size(struct list_node *head);
extern struct list_node *list_find(struct list_node *head, int value, struct list_node **predp);
extern int list_remove(struct list_node **headp, int value);

void
display(struct list_node *head){
	while(head != NULL){
		printf("%d\n", head->value);
		head = head -> next;
	}
}


int
main() {
	// this is the test for list_insert
	struct list_node *newcellptr;
	struct list_node *end_ptr;
	struct list_node **predp;
	struct list_node *item_fount;
	struct list_node **headp;
	int size;
	newcellptr = malloc(sizeof(struct list_node));
	newcellptr -> value = 1;
	newcellptr -> next = NULL;
    list_insert(newcellptr, 2);

	assert(newcellptr->next->value==2);   

	//display(newcellptr);

	// this is the test for list_end

	end_ptr = list_end(newcellptr);

	assert(end_ptr->value == 2);

	end_ptr = list_end(newcellptr);

    list_insert(end_ptr, 5);

	//display(newcellptr);
	end_ptr = list_end(newcellptr);

	assert(end_ptr->value==5);

	// This is the test for list size

	list_insert(end_ptr, 9);

	size = list_size(newcellptr);

	assert(size == 4);


	// This is the test for list find
	// 1 -> 2 -> 5 -> 9

	predp = malloc(sizeof(struct list_node **));
	item_fount = list_find(newcellptr, 9, predp);

	assert( item_fount -> value == 9);
	//printf("%d\n", (*predp) -> value);

	item_fount = list_find(newcellptr, 2, predp);
	//printf("%d\n", (*predp) -> value);

	item_fount = list_find(newcellptr, 1, predp);





	assert( (*predp) == NULL);
	//headp = malloc(sizeof(struct list_node*));
	//*headp = malloc(sizeof(struct list_node)); // segmentation why - I just want a copy of my struct
	//**headp = *newcellptr;

	headp = &newcellptr;
	list_remove(headp, 5); // how do I check this? I lost my reference
	display(*headp);

	printf("all my tests are good!\n");
}