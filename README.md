# CS202 spring 2023 labs

This is the upstream source for student labs in the course Operating Systems at New York University.

For more information about this course, please access:
https://cs.nyu.edu/~mwalfish/classes/23sp


## What is WEENSY OS?

In lab 4, I implemented process memory isolation, virtual memory, and a system call (fork()) in a tiny (but real!) operating system, called WeensyOS.The WeensyOS kernel runs on x86-64 CPUs, so I run WeensyOS in QEMU. QEMU is a software-based x86-64 emulator: it “looks” to WeensyOS just like a physical x86-64 CPU with a particular hardware configuration. 


## How to run WEENSY OS?

1- Download, install and **run** the [docker](https://www.docker.com/) desktop application

2 - Clone this repository `git clone https://gitlab.com/danysigha/kernel_development.git`

3- `$ cd ~/cs202`

4- Activate the Docker container, then run the project:

    $ ./cs202-run-docker

    cs202-user@172b6e333e91:~/cs202-labs$ cd lab4/
    
    cs202-user@172b6e333e91:~/cs202-labs/lab4$ make run

`make run` should cause you to see something like the below, which shows four processes running in parallel, each running a version of the program in p-allocator:

![virt_add_space_phy_mem](https://cs.nyu.edu/~mwalfish/classes/23sp/labs/fig-memos-initial.gif)


**What is the actual software/hardware stack here?**

The answer is different for people with x86-64 computers (for example, Windows machines and older Macs) and ARMs. We all run a host OS (on our computer) on top of either x86-64 or ARM hardware (ARM being the architecture for so-called Apple silicon, namely M1 and M2 chips). Then, the Docker containerization environment runs on top of the host OS (as a process). That environment, loosely speaking, emulates either an x86 or an ARM CPU, and running on top of that emulated CPU is Ubuntu Linux, targeted to x86-64 or ARM. Running on top of Ubuntu is QEMU. QEMU presents an emulated x86-64 interface, and QEMU itself is either an x86-64 or ARM binary, again depending on the underlying hardware. Finally, WeensyOS is exclusively an x86-64 binary, and that of course runs on QEMU. Taking that same progression, now top-down: if you have an ARM CPU, that means you are running the WeensyOS kernel’s x86-64 instructions in QEMU, a software-emulated x86-64 CPU that is an ARM binary, on top of Linux (targeted to ARM), running in the Docker containerization environment (also itself an ARM binary), on macOS, running on an ARM hardware CPU.


## What do the visuals mean?

**Here’s how to interpret the memory map display:**

- WeensyOS displays the current state of physical and virtual memory. Each character represents 4 KB of memory: a single page. There are 2 MB of physical memory in total.

- WeensyOS runs four processes, 1 through 4. Each process is compiled from the same source code (p-allocator.c), but linked to use a different region of memory.

- Each process asks the kernel for more heap memory, one page at a time, until it runs out of room. As usual, each process's heap begins just above its code and global data, and ends just below its stack. The processes allocate heap memory at different rates: compared to Process 1, Process 2 allocates twice as quickly, Process 3 goes three times faster, and Process 4 goes four times faster. (A random number generator is used, so the exact rates may vary.) The marching rows of numbers show how quickly the heap spaces for processes 1, 2, 3, and 4 are allocated.

Here are two labeled memory diagrams, showing what the characters mean and how memory is arranged.

![physmap](https://cs.nyu.edu/~mwalfish/classes/23sp/labs/fig-memos-physmap.gif)
![physmap2](https://cs.nyu.edu/~mwalfish/classes/23sp/labs/fig-memos-physmap2.gif)


**The virtual memory display is similar.**

- The virtual memory display cycles successively among the four processes’ address spaces. In the base version of the WeensyOS code we give you to start from, all four processes’ address spaces are the same (your job will be to change that!).

- Blank spaces in the virtual memory display correspond to unmapped addresses. If a process (or the kernel) tries to access such an address, the processor will page fault.

- The character shown at address X in the virtual memory display identifies the owner of the corresponding physical page.

- In the virtual memory display, a character is highlighted if an application process is allowed to access the corresponding address. Initially, any process can modify all of physical memory, including the kernel. Memory is not properly isolated.

